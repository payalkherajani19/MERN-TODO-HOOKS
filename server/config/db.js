const mongoose = require('mongoose');
const config = require('config');
const connectionURL = config.get('mongoURI');

//Function for connection establishment
const connectDB = async() => {
     try {
         await mongoose.connect(connectionURL,{useNewUrlParser: true,useUnifiedTopology: true,useCreateIndex: true,useFindAndModify: false });
         console.log('MongoDB Connected');
     } catch (error) {
         console.error(error.message);
         process.exit(1);  //Exit the process with failure
     }
}

module.exports = connectDB;
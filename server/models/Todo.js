const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TodoSchema = new mongoose.Schema({
    user:{ 
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    tasks: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }

})

module.exports = Todo = mongoose.model('todo',TodoSchema);
const express = require('express');
const app = express();
const PORT = 5000;

//Connecting mongodb
const connectDB = require('./config/db');
connectDB();

//Init Middleware
app.use(express.json({extended: false}));

//Routing
app.use('/api/user',require('./routes/user'));
app.use('/api/auth',require('./routes/auth'));
app.use('/api/todo',require('./routes/todo'));

app.listen(PORT,() => {
    console.log(`Server started at ${PORT}`);
});
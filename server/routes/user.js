const express = require('express');
const router = express.Router();
const {check,validationResult} = require('express-validator');
const User = require('../models/user');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const auth = require('../middleware/auth');
const Todo = require('../models/Todo');


// @route   POST api/users
// @desc    Register User
// @access  Public

//Register User

router.post('/',[
    check('name','Name is required').not().isEmpty(),
    check('email','Email is required').isEmail(),
    check('password','Enter password with min length 6').isLength({
        min: 6
    })
],async(req,res) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json({ errors: errors.array()})
    }

    const { name, email, password } = req.body;

    try {
        
        //See if user exists
        let user = await User.findOne({ email });
        if(user){
            return res.status(400).json({ errors: [{ msg: 'User already exists '}]})
        }
        
        //Create a instance of User model and add a object {}
        user = new User({
            name,
            email,
            password
        })
       
        //Encrypt password
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(password,salt);

        await user.save();
        const payload = {
            user: {
                id: user.id
            }
        }

        jwt.sign(payload,
            config.get('jwtSecret'),
            { expiresIn: 36000 },
            (err,token) => {
                if(err){
                    throw err;
                }
             res.json({ token });
            }

        );


    } catch (error) {
        // console.log(err.message);
        res.status(500).send('Server Error ');   
    }

})


// @route   DELETE api/user
// @desc    Delete User + all post
// @access  Private

router.delete('/',auth,async(req,res) => {
    try {
        await Todo.deleteMany({user: req.user.id});
        await User.findOneAndRemove({ _id: req.user.id });
        res.json({ msg: 'User Removed'});
    } catch (error) {
        // console.error(error.message);
        res.status(500).send("Server Error");
    }
})

module.exports = router;
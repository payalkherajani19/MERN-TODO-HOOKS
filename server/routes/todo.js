const express = require('express');
const Todo = require('../models/Todo');
const router = express.Router();
const auth = require('../middleware/auth.js');
//Validating data
const { check,validationResult} = require('express-validator');



// @route   POST api/todo
// @desc    add todos
// @access  Private

router.post('/',[auth,[
    check('tasks','Tasks is required').not().isEmpty()
]],async(req,res) => {
try {

    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json({ errors: errors.array()});
    }
     const todo = new Todo({
        tasks: req.body.tasks,
        description: req.body.description,
        user: req.user.id
    });

    await todo.save();

    res.send(todo);
} catch (error) {
    // console.error(error.message);
    res.status(500).send('Server Error');
}
});


// @route   GET api/todo/alltodos
// @desc    get all your todos
// @access  Private

router.get('/alltodos',auth,async(req,res) => {
    try {
        const todos = await Todo.find().sort({ date: -1});
        res.json(todos);
    } catch (error) {
        // console.error(error.message);
        res.status(500).send('Server Error');
    }
})

// @route   PUT api/todo/:todoid
// @desc    Update a todo(Edit a todo item)
// @access  Private

router.put('/:todoid',[auth,[
    check('tasks','Please add a new task to update').not().isEmpty()
]],async(req,res) => {
    try {
    
        const errors = validationResult(req);
       
        if(!errors.isEmpty()){
            return res.status(400).json({ errors: errors.array()})
        }

        let todo = await Todo.findById(req.params.todoid);
        
        const updatedTask = {
            tasks: req.body.tasks,
            description: req.body.description
        }
            todo = await Todo.findOneAndUpdate({
                user: req.user.id
            }, {$set: updatedTask},{new: true})

        res.send(todo);
    } catch (error) {
        // console.error(error.message);
        res.status(500).send('Server Error');
    }
})


// @route   DELETE api/todo/:todoid
// @desc    Delete a todo item
// @access  Private

router.delete('/:todoid',auth,async(req,res) => {
   try {
      
     const todoitem = await Todo.findById(req.params.todoid);
     
     if(!todoitem){
         return res.status(400).json({msg: "No such Todoitem exists"})
     }

     //authorize user to delete post
     if(todoitem.user.toString() !== req.user.id){
         return res.status(401).json({msg: 'User not authroized'});
     }

     await todoitem.remove();
     res.json({msg: "Todo item removed"});
      
   } catch (error) {
    //    console.error(error.message);
       res.status(500).send('Server Error');
   }
});


// @route   DELETE api/todo/todos
// @desc    Delete all todos
// @access  Private

router.delete('/',auth,async(req,res) => {
    
    try {
       await Todo.deleteMany({user: req.user.id})
        res.json({ msg: 'All todos Removed'})
    } catch (error) {
        // console.error(error.message);
        res.status(500).send("Server Error");
        
    }

});
module.exports = router;
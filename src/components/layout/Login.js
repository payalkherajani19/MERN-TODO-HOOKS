import React from 'react';
//component
import Navbar from './Navbar';

//image
import loginuser from '../../images/login.svg';
const Login = () => {
    return (
        <div className="container-fluid ">
           <Navbar logout={false} />
           <div className="container-fluid d-flex">
                <div className="container mt-5 d-flex d-none d-sm-block align-items-center align-self-center ">
                    <img src={loginuser} className="useraddimage d-none d-sm-block  " />
                </div>
                <div className="container-fluid mt-5 p-1 register-div-2">
                    <h1 className="mb-2 pb-2" >Login</h1>
                    <form>

                        <div class="mb-3">
                            <input type="email" class="form-control" placeholder="Your Email Address" />
                        </div>

                        <div className="mb-3">
                            <input type="password" className="form-control" placeholder="Password" />
                        </div>
 
                        <button type="submit" class="btn btn-dark">Login</button>
                     </form>
                </div>

           </div>
        </div>
    )
}



export default Login;
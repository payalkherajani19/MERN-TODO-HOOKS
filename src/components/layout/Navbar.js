import React from 'react';
import { Link } from 'react-router-dom';

const Navbar = (props) => {
        return(
            <nav className="navbar navbar-expand-sm bg-dark mt-1">
                   <div className="container-fluid p-2">
                       <div className="display-6 d-flex text-light pr-1"> 
                       <Link className="navbar-brand text-light display-6" to='/'><i className="fas fa-clipboard-list fa-2x"></i> </Link> 
                       TODO 
                       </div>
                        
                        {
                            props.logout === true ? (
                                <div className="collapse navbar-collapse justify-content-end m-1" id="navbarNav">
                                        <ul className="navbar-nav">
                                            <li className="nav-item">
                                                <Link className="nav-link text-light" to='/'> <i className="fas fa-sign-out-alt"></i>Logout</Link>
                                            </li>
                                        </ul>
                                </div>
                            ) : (
                                <div className="collapse navbar-collapse justify-content-end m-1" id="navbarNav">
                                    <ul className="navbar-nav">
                                        <li className="nav-item">
                                            <Link className="nav-link text-light" to='/register'>Register</Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link className="nav-link text-light" to='/login'>Login</Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link className="nav-link text-light" to='/todo'>Dashboard</Link>
                                        </li>
                                    </ul>
                                </div>
                            )
                        }
                   
                    </div>
               </nav>
        )
    }


export default Navbar;
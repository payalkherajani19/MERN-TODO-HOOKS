import React,{useState} from 'react';
//import component
import TodoElement from './TodoElement';
import Navbar from '../layout/Navbar'

const Todo = () => {
    const [todos,setTodos] = useState([]);
    const [tasks,setTasks] = useState("");

    //Functions defined

    const inputHandler = (event) => {
        const inputValue = event.target.value;
        setTasks(inputValue);
    }

   const addTodoFunction = () =>{
        const newTask = {
            id: Date.now(),
            tasks: tasks,
        }

        const newTodoItem = [...todos,newTask];
        setTodos(newTodoItem);
        setTasks("");
    }

    const editTodo = (newVal,id) =>{
       const editTodoUpdate = todos.map((item) => {
           if(item.id === id){
              item.tasks=newVal
           }
           return item
           
       })
       setTodos(editTodoUpdate);
    }
   
   const deleteTodoItem = (id) => {
      const deleteTodoUpdate = todos.filter((item) => item.id!== id);
      setTodos(deleteTodoUpdate);
    }

        return (
            <div className="container-fluid h-100">

                {/* Navbar */}
                <Navbar logout={true} />

               {/* Section */}
               <section className="d-flex flex-column">
                   {/* Add Todo */}
                   <div className="container mt-5 d-flex justify-content-center">
                       <input type="text" onChange={inputHandler} value={tasks} className="todo-input" placeholder="what task you want to add ?" />
                       <button onClick={addTodoFunction} className="btn btn-secondary">Add Todo</button>
                   </div>

                   <div className="container mt-5 d-flex flex-column align-items-center">
                   {
                       todos.map((singleTodo) => {
                           return(
                           <div key={singleTodo.id}>
                               <TodoElement
                               singleTodoItem={singleTodo}
                               editFunction={editTodo}
                               deleteFunction={deleteTodoItem}
                               />   
                           </div>
                           ) 
                       })
                   }
                   </div> 
               </section>
            </div>
        )
    
   
}

export default Todo;
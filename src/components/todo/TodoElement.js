import React,{useState} from 'react';

const TodoElement = (props) => {
    const [editing,setEditing] = useState(false);
    const [newVal,setNewVal] = useState(props.singleTodoItem.tasks);

        return(
            <div className="todoelement">
                {
                    editing === true ? 
                    (
                        <div className="p-2 m-1 d-flex">
                             <input type="text" value={newVal} onChange={(e) => setNewVal(e.target.value)} className="form-control m-1 " />
                             <button className="btn btn-dark m-1" onClick={() => {
                                 props.editFunction(newVal,props.singleTodoItem.id)
                                 setEditing(false);
                                }
                             }>Update</button>     
                       </div>
                    ) : 
                    ( 
                    <div className="d-flex">  
                         <ul className="p-3 m-1">
                            <li> {props.singleTodoItem.tasks}</li>
                        </ul>
                         <div className="m-1 p-2"> 
                             <button onClick={() => setEditing(true)} className="btn btn-success m-1"><i className="far fa-edit"></i></button>
                             <button onClick={() => props.deleteFunction(props.singleTodoItem.id)} className="btn btn-danger m-1"><i class="far fa-trash-alt"></i></button>
                         </div>
                       
                      
                    </div>
                    
                    
                    )
                }
               
                
            </div>
        )
    
}

export default TodoElement;
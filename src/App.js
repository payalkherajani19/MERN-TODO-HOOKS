//React imports
import React from 'react';
import { BrowserRouter as Router, Switch,Route } from 'react-router-dom';
//CSS File Import
import './App.css';
//Components import 
import Landing from './components/layout/Landing';
import Register from './components/layout/Register';
import Login from './components/layout/Login';
import Todo from './components/todo/Todo';


const App = () => {
  return (
    <Router>
        <Switch>
             <Route path='/' exact component={Landing} />
             <Route path='/register' exact component={Register} />
             <Route path='/login' exact component={Login} />
             <Route path='/todo' exact component={Todo} />    
        </Switch>
    </Router>
  );
}

export default App;
